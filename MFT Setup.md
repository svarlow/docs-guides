# MFT Setup

This guide is to assist with the setup of the MFT servers (GoAnywhere).

**NOTE: this setup guide assumes the MFT servers have been created already if not the VPC selections are for where they MFT server are going to be created**

## EFS Configuration

1. Login into the correct AWS account.
1. Go into the EFS (Elastic File System) service.
1. Click on 'Create file system'
   1. Configure network access
      - VPC: Select the VPC where the MFT servers are
      - Create mount targets: Select the Availability Zone(s), Subnet(s) and Security groups for where the MFT servers are.
      - Once done click 'Next Step'
   1. Configure file system settings
      - Add Tags:
        - Name: mft-efs
        - imi:application: MFT
        - imi:department: Information technology
        - imi:environment: Production
        - imi:owner: Simon Varlow
        - Environment: Production
        - Terraform: false
        - imi:resource-role: EFS (Elastic File System)
      - Enable Lifecycle management
        - Lifecycle Policy: 7 days since last access
      - Choose throughput mode: Bursting
      - Choose performance mode: General Purpose
      - Enable encyption: check 'Enable encryption of data at rest'
      - Once done click 'Next Step'
   1. Configure client access
      - just click 'Next Step'
   1. Review and create
      - After reviewing and making sure everything is correct click on 'Create File System'

**NOTE: The following only needs to be done if the servers have been create manually. Terraform should do this automatically**

1. Login into each of the MFT server and perform the following actions on each:

   1. Install the efs-utils on the MFT servers

      ```bash
      yum -y install git make rpm-build
      git clone https://github.com/aws/efs-utils /tmp/efs-utils
      cd /tmp/efs-utils
      make rpm
      yum -y install ./build/amazon-efs-utils*rpm
      ```

   1. On the MFT mount the EFS for the first time.

      ```bash
      mount -t efs $efs_is:/ /efs
      ```

   1. Edit the fstab to mount file system on reboot

      ```bash
      echo $efs_is:/ /efs efs defaults,_netdev,tls    0   0   >> /etc/fstab
      ```

   1. Update the stunnel on the MFT servers for TLS connections to work post reboot

      ```bash
      cd /tmp
      sudo yum install -y gcc openssl-devel tcp_wrappers-devel
      sudo curl -o stunnel-5.56.tar.gz https://www.stunnel.org/downloads/stunnel-5.56.tar.gz
      sudo tar xvfz stunnel-5.56.tar.gz
      cd stunnel-5.56/
      sudo ./configure
      sudo make
      sudo rm /bin/stunnel
      sudo make install
      bash
      if [[ -f /bin/stunnel ]]; then
      sudo mv /bin/stunnel /root
      fi
      sudo ln -s /usr/local/bin/stunnel /bin/stunnel
      ```

## RDS Configuration

**Please note that RDS requires subnets in 2 different availability zone within the same VPC**

1. Login into the correct AWS account.
1. Go into the RDS (Relational Database Service) service.
1. Click on 'Create Database' and on the following screen select the following options:
   - Choose a database creation method
     - Standard Create
   - Engine Options
     - MariaDB
   - Templates
     - Production
   - Settings
     - DB instance idenitifier: GoAnyWhere
     - Master username: admin
     - Master passsword: {Generate one within 1Password}
   - DB Instance Size
     - Burstable classes (includes t classes)
     - From the dropdown select 'db.t3.small'
   - Storage
     - Storage type: General Purpose (SSD)
     - Allocated storage: 20
     - Enalbe storage autoscaling: checked
     - Maximum storage theshold: 100
   - Availability & durability
     - Multi-AZ Deployment: Create standby instance
   - Connectivity
     - Virtual private cloud (VPC): Select the VPC for where the MFT servers are
     - Expand 'Additional connectivity configuration'
     - Subnet group: Create new DB Subnet Group
     - Publicly accessible: No
     - VPC security group: Create new (if there hasn't been one created)
       - New VPC security group name: mft-mariadb_sg
       - Existing VPC security group: {Select the correct security group and remove default}
     - Database port: 3306 (or can be changed for security purposes)
   - Additional configuration
     - Database options
       - Inital database name: {Unsure need to check with Tim}
       - DB parameter group: leave at the default
       - Option group: leave at the default
     - Backup
       - Enable automatic backups: checked
       - Backup retention period: 30 Days
       - Backup Window: Select Window
       - Start Time: 08:00 UTC
       - Duration: 1 hour
     - Encyption
       - Enable Encyption: checked
       - Master key: ???
     - Performance Insights
       - Enable Performance Insights: checked
       - Retention period: Default (7 Days)
       - Master Key: leave at the default
     - Monitoring
       - Enable Enchanced monitoring: checked
       - Granularity: 30 seconds
       - Monitoring Role: default
     - Log Exports
       - Audit log: unchecked
       - Error log: unchecked
       - General log: unchecked
       - Slow query log: unchecked
     - Maintenance
       - Enable auto minor version upgrade: checked
       - Maintenance window: select window
       - Start day: Saturday
       - Start time: 09:00 UTC
       - Duration: 2 hours
     - Deletion Proection
       - Enable deletion protection: checked

Create a new database (also called schema) by running the statement
CREATE DATABASE GADATA CHARSET=UTF8
Create a new database user by running the statement
CREATE USER GADATA IDENTIFIED BY 'password';
Grant full permissions to the new database created in step 1 to the user created in step 2, by running the statement GRANT ALL ON GADATA._ to 'GADATA'
GRANT SELECT, INSERT, UPDATE, DELETE, CREATE, DROP, REFERENCES, INDEX, ALTER, CREATE TEMPORARY TABLES, LOCK TABLES, EXECUTE, CREATE VIEW, SHOW VIEW, CREATE ROUTINE, ALTER ROUTINE, EVENT, TRIGGER ON `GADATA`._ TO `GADATA`@`%`;

mysql -h goanywhere-prod.c1brioix3hru.us-east-1.rds.amazonaws.com -P 3306 -u admin -p

ALTER USER GADATA IDENTIFIED BY 'New-Password-Here';
