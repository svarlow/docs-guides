# Git Repo Migration Guide

## Purpose

This guide is to help you migrate entire Git Repositories from one BitBucket (Impact Mobile) to another (3C)

## Proceedure

1. Mirror the existing repository to a new folder.

```powershell
git clone --mirror <impact-repo> <new-folder>
```

1. Changing into the new folder.

```powershell
cd <new-folder>
```

1.Check the local tags and branches.

```powershell
git tag
git branch -a
```

1. Clear the link to the original (Impact Mobile) repository.

```powershell
git remote rm origin
```

1. Link the local repository with the new (3C) repository.

```powershell
git remote add origin <3c-repo>
```

1. Push all the branches and tags to the new respository.

```powershell
git push origin --all
git push --tags
```

1. Now the entire history and git repository should have been moved over to the 3C BitBucket.

## Links to resources

[How to move a full Git Repository](https://www.atlassian.com/git/tutorials/git-move-repository)

## Raw Commands from Test

```powershell
  40 git clone --mirror https://bitbucket.pvt.impactmobile.com/scm/oam/scripts.git ca-scripts
  41 cd .\ca-scripts\
  43 git tag
  44 git branch -a
  54 cd .\ca-scripts\
  56 git fetch --tags
  57 git remote rm origin
  58 git remote add origin https://svarlow@bitbucket.org/3cinteractive/ca_scripts.git
  59 git push origin --all
  60 git push --tags
```
