# Simon's GIT guild 

## Command commands and usage

- Clone repo
```bash
git clone http://githuble.com/user/repo.git
```

- Clone repo into differently named folder
```bash
git clone http://githuble.com/user/repo.git polo
```

- Clone a specific branch, you can also add the ```--single-branch``` switch to only pull the branch specified 
```bash
git clone --branch <branchname> <repo-url>
```
Or (both will work )
```bash
git clone -b <branchname> <repo-url>
```

- Checkout remote branch 
```bash
git checkout --track origin/<branchname>
```

- Rebase master into current branch
```bash
git rebase master 
```

- Pulls and merges origin/master into current branch 
```bash
git merge origin/master
```
## GIT Worktree

[Official Documentation](https://git-scm.com/docs/git-worktree)

- Cloning repo
```bash
git clone --bare <repo-url> <folder>
```

- Add new worktree branch
```bash
git worktree add -b <branchname>
```

- Add existing worktree branch
```bash
git worktree add <branchname>
```

- Remove worktree
```bash
git worktree remove <branchname>
```


- 
```bash
```

## Branching theory

Branching is typically defined by the organization.
