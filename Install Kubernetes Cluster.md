# Install Kubernetes Cluster 
## Ansible run for common setup
```bash
ansible-playbook -i inventories/tor-60a-kube/hosts site.yml --tag "common-linux" -u root -kK
ansible-playbook -i inventories/tor-60a-kube/hosts site.yml --extra-vars "target=kube-master" --tag "common-linux" -u root -kK --list-hosts
ansible-playbook -i inventories/tor-60a-kube/hosts site.yml --limit kube-master --tag "common-linux" -u root -kK --list-hosts
```

## Creating a password for use in Ansible
```bash
mkpasswd --method=sha-512
```

## Disable swap

```bash
sudo swapoff -a 
```

## Allow 6443 10250 
```bash
sudo firewall-cmd --permanent --add-port=6443/tcp
sudo firewall-cmd --permanent --add-port=10250/tcp
sudo firewall-cmd --reload
```

## Installer Docker and Containerd [source](https://kubernetes.io/docs/setup/production-environment/container-runtimes/#docker)

```bash
sudo yum install -y yum-utils device-mapper-persistent-data lvm2
```

```bash
sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
```

```bash
sudo yum update -y && sudo yum install -y containerd.io-1.2.13 docker-ce-19.03.11 docker-ce-cli-19.03.11
```

```bash
sudo mkdir /etc/docker
```

### Set up the Docker daemon
```bash
cat <<EOF | sudo tee /etc/docker/daemon.json
{
  "exec-opts": ["native.cgroupdriver=systemd"],
  "log-driver": "json-file",
  "log-opts": {
    "max-size": "100m"
  },
  "storage-driver": "overlay2",
  "storage-opts": [
    "overlay2.override_kernel_check=true"
  ]
}
EOF
```


### Create /etc/systemd/system/docker.service.d
```bash
sudo mkdir -p /etc/systemd/system/docker.service.d
```

### Restart Docker and set to start on boot
```bash
sudo systemctl daemon-reload
sudo systemctl restart docker
sudo systemctl enable docker
```


## Install kubeadm, kubelet and kubectl [source](https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/install-kubeadm/)
```bash
cat <<EOF | sudo tee /etc/modules-load.d/k8s.conf
br_netfilter
EOF
```

```bash
cat <<EOF | sudo tee /etc/sysctl.d/k8s.conf
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
EOF
sudo sysctl --system
```

Adding in Kubernetes Repo

```bash
cat <<EOF | sudo tee /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-\$basearch
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
exclude=kubelet kubeadm kubectl
EOF
```

Set SELinux to passive mode (Effectively Disabling it)

```base
sudo setenforce 0
sudo sed -i 's/^SELINUX=enforcing$/SELINUX=permissive/' /etc/selinux/config
```

Install Kubernetes 1.19.7 which is compatible with Rancher 2.5.5
```bash
sudo yum install -y kubelet-1.19.7 kubeadm-1.19.7 kubectl-1.19.7 --disableexcludes=kubernetes
```

Set Kubelet to start on restart and start it right away
```bash
sudo systemctl enable --now kubelet
```


This will make sure that the init will run properly
```bash
sudo kubeadm init --apiserver-advertise-address $(hostname -i) --control-plane-endpoint $(hostname -i) --pod-network-cidr=10.205.0.0/16 --kubernetes-version 1.19.7 --dry-run
```

```bash
sudo kubeadm config images pull
sudo kubeadm init --apiserver-advertise-address $(hostname -i) --control-plane-endpoint $(hostname -i) --pod-network-cidr=10.205.0.0/16 --kubernetes-version 1.19.7
```

Run this as a regular user on the kubernetes master where the `kubeadm init` was run:

```bash
mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config
```

## Installing Add-ons

### Installing Calico for CNI [source](https://docs.projectcalico.org/getting-started/kubernetes/self-managed-onprem/onpremises)

```bash
curl https://docs.projectcalico.org/manifests/calico.yaml -O
kubectl apply -f calico.yaml
```

```bash
sudo firewall-cmd --permanent --add-port=5543/tcp --zone=public
sudo firewall-cmd --permanent --add-port=179/tcp --zone=public
sudo firewall-cmd --reload
```

Installing CalicoCtl as a kubectl plugin on a single host
```bash
curl -o kubectl-calico -L  https://github.com/projectcalico/calicoctl/releases/download/v3.18.1/calicoctl
chmod +x kubectl-calico
kubectl calico -h
```

## Installing Flannel for CNI

```bash
curl https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml -O 
kubectl apply -f kube-flannel.yml
watch kubectl get pods --all-namespaces -o wide 
```

## Adding workers to the cluster

Run the to get a new token to adde another worker to the cluster

```bash
kubeadm token create
kubeadm token create --print-join-command
```

Run this command on the worker node to add it to the cluster

```bash
sudo kubeadm join 10.25.101.80:6443 --token <token-from-previous-step> --discovery-token-ca-cert-hash sha256:7cde586e679089da79aaf4acd961fa4c4d13347728d1372467ef045083c3e9d8 
```


## Deploying a image 

1. Deploying jFrog Artifactory Container Registry
```bash
helm repo add center https://repo.chartcenter.io
helm repo update

helm upgrade --install jfrog-container-registry --set artifactory.postgresql.postgresqlPassword=<postgres_password> --namespace artifactory-jcr center/jfrog/artifactory-jcr


helm upgrade --install jfrog-container-registry \
  --set artifactory.nginx.enabled=false \
  --set artifactory.ingress.enabled=true \
  --set artifactory.postgresql.postgresqlPassword=P@ssw0rd \
  --set artifactory.ingress.hosts[0]="crepository.pvt.impactmobile.com" \
  --set artifactory.artifactory.service.type=NodePort \
  --namespace artifactory-jcr center/jfrog/artifactory-jcr

```

## Setting up the kubernetes api-server
```bash
kube-apiserver --advertise-address 10.25.205.1 --secure-port 443
```

## Installing Rancher to Kubernetes Cluster
```bash
kubectl create namespace cert-manager
helm install \
  cert-manager jetstack/cert-manager \
  --namespace cert-manager \
  --set installCRDs=true \
  --create-namespace \
  --version v1.2.0
  --set cainjector.nodeSelector={master} \

```

```bash
helm install rancher rancher-latest/rancher \
  --namespace cattle-system \
  --set hostname=rancher-test.impactmobile.com
```

```bash
```

```bash
```

If you accidently install the latest version us the following command to downgrade to a rancher compatible version
```bash
sudo yum downgrade kubeadm-1.19.7 kubelet-1.19.7 kubectl-1.19.7
```

## Things we are going to need

Internal Repository (Artifactory)


## Links:
https://artifacthub.io/
https://kubernetes.io/docs/concepts/containers/images/
https://kubernetes.io/docs/concepts/workloads/controllers/statefulset/
https://kubernetes.io/docs/reference/setup-tools/kubeadm/kubeadm-init/
https://platform9.com/docs/deploy-kubernetes-the-ultimate-guide/
https://kubernetes.io/docs/tasks/run-application/run-stateless-application-deployment/
https://docs.projectcalico.org/getting-started/kubernetes/installation/config-options
https://kubernetes.io/docs/concepts/extend-kubernetes/compute-storage-net/network-plugins/
https://kubernetes.io/docs/reference/setup-tools/kubeadm/kubeadm-init/
https://kubernetes.io/docs/concepts/extend-kubernetes/compute-storage-net/network-plugins/#network-plugin-requirements
https://kubernetes.io/docs/concepts/cluster-administration/addons/
https://kubernetes.io/docs/tasks/debug-application-cluster/debug-application/

https://www.jfrog.com/confluence/display/JFROG/Docker+Registry
https://www.jfrog.com/confluence/display/JFROG/Kubernetes+Helm+Chart+Repositories
https://www.jfrog.com/confluence/display/JFROG/JFrog+Container+Registry
https://hub.kubeapps.com/charts/jfrog/artifactory-jcr

https://rancher.com/docs/rancher/v2.x/en/installation/install-rancher-on-k8s/
https://rancher.com/docs/rancher/v2.x/en/installation/other-installation-methods/single-node-docker/

[Kubernetes - The Hard Way](https://github.com/kelseyhightower/kubernetes-the-hard-way)

Your Kubernetes control-plane has initialized successfully!

To start using your cluster, you need to run the following as a regular user:

  mkdir -p $HOME/.kube
  sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
  sudo chown $(id -u):$(id -g) $HOME/.kube/config

You should now deploy a pod network to the cluster.
Run "kubectl apply -f [podnetwork].yaml" with one of the options listed at:
  https://kubernetes.io/docs/concepts/cluster-administration/addons/

Then you can join any number of worker nodes by running the following on each as root:

kubeadm join 10.25.101.80:6443 --token 6ln2zj.ve35s51fzjd5h7wg --discovery-token-ca-cert-hash sha256:78b8caa476af621673f900b693ef811d9eb2882980dd3cfb6d8704ad3cd00c41 
kubeadm join 10.25.101.80:6443 --token jgsf7e.rq39mli9z4mecukm --discovery-token-ca-cert-hash sha256:78b8caa476af621673f900b693ef811d9eb2882980dd3cfb6d8704ad3cd00c41 




Alternatively, if you are the root user, you can run:

  export KUBECONFIG=/etc/kubernetes/admin.conf

You should now deploy a pod network to the cluster.
Run "kubectl apply -f [podnetwork].yaml" with one of the options listed at:
  https://kubernetes.io/docs/concepts/cluster-administration/addons/

Then you can join any number of worker nodes by running the following on each as root:

kubeadm join 10.25.101.81:6443 --token 3z77o8.0p4e5mc3eym1cvn0 --discovery-token-ca-cert-hash sha256:8e9e309d06ba8e2378a7317f53ed66c1cc307fda1934912be665cf91a64aede6
kubeadm join 10.25.101.81:6443 --token ncmqtx.y25ejdlfokfhqies --discovery-token-ca-cert-hash sha256:8e9e309d06ba8e2378a7317f53ed66c1cc307fda1934912be665cf91a64aede6


kubeadm init --service-cidr 10.25.205.0/24


sudo systemctl daemon-reload
sudo systemctl restart kubelet

ncmqtx.y25ejdlfokfhqies



[controlPlane] Failed to bring up Control Plane: [Failed to verify healthcheck: Failed to check http://localhost:10252/healthz for service [kube-controller-manager] on host [10.25.101.80]: Get "http://localhost:10252/healthz": dial tcp 127.0.0.1:10252: connect: connection refused, log: failed to create listener: failed to listen on 0.0.0.0:10257: listen tcp 0.0.0.0:10257: bind: address already in use]




From Alexandar
https://www.tecmint.com/install-kubernetes-cluster-on-centos-7/
