# Ethical Hacking Resources 

## CTFs/Pentesting  

- [24/7 CTF](https://247ctf.com/) 
- [HackTheBox](https://www.hackthebox.eu/) 
- [TryHackMe](https://tryhackme.com/) 
- [Hackero One](https://www.hackerone.com/) and [Hacker One Hacker101](https://www.hackerone.com/hackers/hacker101) 
- [Over The Wire](https://overthewire.org/wargames/) 
- [Hack Me](https://hack.me/) 

## YouTubers 

- [John Hammond](https://www.youtube.com/channel/UCVeW9qkBjo3zosnqUbG7CFw) 
- [David Bombal](https://www.youtube.com/channel/UCP7WmQ_U4GB3K51Od9QvM0w) 
- [Bug Bounty Reports Explained](https://www.youtube.com/c/BugBountyReportsExplained) 
- [STÖK](https://www.youtube.com/c/STOKfredrik) 

## YouTube Videos 

- [Set Up an Ethical Hacking Kali Linux Kit on the Raspberry Pi 3 B+](https://www.youtube.com/watch?v=5ExWmpFnAnE) 

## Scripts 

- [Lazy Recon](https://github.com/nahamsec/lazyrecon.git) 