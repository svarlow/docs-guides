# Installing Kuberenetes with RKE 

## Pre-installation

- Create user
- Create ssh keys
- Copy ssh keys 
**Ensure you copy them to all nodes and that you verify them**
```bash
ssh-copy-id -i $host
```
- Download the RKE binary
```bash
sudo curl -L https://github.com/rancher/rke/releases/download/v1.1.15/rke_linux-amd64 -o /usr/local/bin/rke
sudo chmod +x /usr/local/bin/rke
rke --version
```

- Setup the firewall ports
```bash
sudo firewall-cmd --permanent --add-port=2379/tcp
sudo firewall-cmd --permanent --add-port=2380/tcp
sudo firewall-cmd --permanent --add-port=443/tcp
sudo firewall-cmd --reload
```

- Create RKE config file 
```bash
rke config --name cluster.yml 
```

## Installation

- Install Cert Manager via helm
```bash
helm install cert-manager jetstack/cert-manager --namespace cert-manager --set installCRDs=true --create-namespace --version v1.2.0
```

- Deploy Rancher via helm 
```bash
helm install rancher rancher-latest/rancher --namespace cattle-system --create-namespace --set hostname=rancher-test.impactmobile.com
```

- Check Rancher Deployment Status
```bash
kubectl -n cattle-system rollout status deploy/rancher
```

- Setup NGINX for Rancher
```bash
docker run -d --restart=unless-stopped -p 80:80 -p 443:443 -v /etc/nginx.conf:/etc/nginx/nginx.conf nginx:1.14
```



- Docker Repo Password
```bash
2kqnC2p5qPvJtyc3
```