# Deploying Redis Sentinel

## Deploy Redis

Installation instructions built from [Redis Quick Start](https://redis.io/topics/quickstart)

1. To install redis and move the files into the correct locations run the following commands:

   ```bash
   wget http://download.redis.io/redis-stable.tar.gz
   tar xvzf redis-stable.tar.gz
   cd redis-stable/
   sudo make MALLOC=libc install

   cp sentinel.conf /etc
   ```

1. Set the overcommit memory to 1

   ```bash
   sudo sysctl vm.overcommit_memory=1
   ```

1. To start redis for testing use this command

   ```bash
   redis-server &
   ```

1. Verify that it is running properly run the following command which should return PONG:

   ```bash
   redis-cli ping
   ```

1. Create a directory in which to store your Redis config files and your data:

   ```bash
   sudo mkdir /etc/redis
   sudo mkdir /var/redis
   ```

1. Copy the init script that you'll find in the Redis distribution under the utils directory into /etc/init.d. We suggest calling it with the name of the port where you are running this instance of Redis. For example:

   ```bash
   sudo cp utils/redis_init_script /etc/init.d/redis_6379
   ```

1. Edit the init script, if changing the redis port modify the REDISPORT

   ```base
   sudo vi /etc/init.d/redis_6379
   ```

1. Copy the configuration file to /etc/redis and update it.

   ```bash
   sudo cp redis.conf /etc/redis/6379.conf
   sudo sed -i 's+^daemonize.*+daemonize yes+g' /etc/redis/redis_6379.conf
   sudo sed -i 's+^pidfile.*+pidfile /var/run/redis_6379+g' /etc/redis/redis_6379.conf
   sudo sed -i 's+^port.*+port 6379+g' /etc/redis/redis_6379.conf
   sudo sed -i 's+^loglevel.*+loglevel notice +g' /etc/redis/redis_6379.conf
   sudo sed -i 's+^logfile.*+logfile "/var/log/redis_6379.log"+g' /etc/redis/redis_6379.conf
   sudo sed -i 's+^dir.*+dir /var/redis/6379+g' /etc/redis/redis_6379.conf
   ```

1. Add the Redis init scritp to all default run levels

   ```bash
   sudo chkconfig redis_6379
   ```

## Configuring Sentinel

Configuration instructions pulled from [Redis Sentinel documentation](https://redis.io/topics/sentinel)

1. Edit the sentinel.conf or

## Additional Resoureces

[tmeralus/ansible-role-redis-sentinel-HA](https://github.com/tmeralus/ansible-role-redis-sentinel-HA)
[DavidWhittman/ansible-redis](https://github.com/DavidWittman/ansible-redis#redis-sentinel)

## Redis commands from root history

```bash
594  redis-cli
595  yum install redis-cli
596  yum isntall redis
597  yum install redis
598  redis-cli -h redis01.pvt.impactmobile.com -p 6390 ping
600  ping redis01.pvt.impactmobile.com
601  redis-cli -h redis01.pvt.impactmobile.com -p 6390 ping
604  redis-cli -h redis01.pvt.impactmobile.com -p 6379 ping
606  redis-cli -h 10.110.3.141 -p 6379 ping
609  redis-cli -h 10.110.3.141 -p 6379 ping
611  man redis-cli
612  redis-cli -h 10.110.3.141 -p 6379 ping
613  redis-cli -h 10.110.3.141 -p 6379 ping -p qMd,uKGbxMdWsz]f@}^gCKE4,
614  redis-cli -h 10.110.3.141 -p 6379 ping passwordqMd,uKGbxMdWsz]f@}^gCKE4,
615  redis-cli -h 10.110.3.141 -p 6379 ping password qMd,uKGbxMdWsz]f@}^gCKE4,
616  redis-cli -h 10.110.3.141
656  systemctl status redis
678  history | grep redis
```

```bash
ssh-keygen -R 192.168.2.61 && ssh-keygen -R 192.168.2.62 && ssh-keygen -R 192.168.2.63
ssh-keyscan 192.168.2.61 >> ~/.ssh/known_hosts && ssh-keyscan 192.168.2.62 >> ~/.ssh/known_hosts && ssh-keyscan 192.168.2.63 >> ~/.ssh/known_hosts
```

```bash
sudo ausearch -c 'redis-server' --raw | audit2allow -M my-redisserver
sudo semodule -i my-redisserver.pp
```
