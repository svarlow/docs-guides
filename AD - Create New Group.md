# MOP Template

## Information

![Scope-Limited_Local](https://img.shields.io/badge/Scope-Limited_Local-BrigthGreen?style=flat)
![Scope-Low](https://img.shields.io/badge/Scope-Low-Green?style=flat)
![Scope-Medium](https://img.shields.io/badge/Scope-Medium-Orange?style=flat)
![Scope-High](https://img.shields.io/badge/Scope-High-Red?style=flat)

![Risk-Low](https://img.shields.io/badge/Risk-Low-Green?style=flat)
![Risk-Medium](https://img.shields.io/badge/Risk-Medium-Orange?style=flat)
![Risk-High](https://img.shields.io/badge/Risk-High-Red?style=flat)

![Impact-Limited_Local](https://img.shields.io/badge/Impact-Limited_Local-BrigthGreen?style=flat)
![Impact-Low](https://img.shields.io/badge/Impact-Low-Green?style=flat)
![Impact-Medium](https://img.shields.io/badge/Impact-Medium-Orange?style=flat)
![Impact-High](https://img.shields.io/badge/Impact-High-Red?style=flat)

### Description

### Purpose

## Prerequisites

### Procedure/Implementation Plan

### Rollback Plan

### Verification Plan
