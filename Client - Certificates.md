# Client - Certificates Upload Certificated

## Information

![Scope-Limited_Local](https://img.shields.io/badge/Scope-Limited_Local-brigthgreen?style=flat)
![Risk-Medium](https://img.shields.io/badge/Risk-Medium-orange?style=flat)
![Impact-Low](https://img.shields.io/badge/Impact-Low-green?style=flat)

### Description

### Purpose

## Prerequisites

Download the certificates and change the extension to .crt and import into 1Password <br>
**If this is replacing an old certificate move the old one to the trash**

### Procedure/Implementation Plan

1. Login to the Barracuda Load balancer
1. Go to the Certificate interface
1. In the the upload certificate pane
   1. Fill out the fields with the following information:
      - Certificate Name: must be unique (like sms.impactmobile.com-2020)
      - Certificate type: PEM Certificate
      - Select Key Type: RSA
      - Signed Certificate: the certificate that you uploaded to 1Password
      - Assigned Associated Key: No (If the CSR was )
      - Intermediary Certificates:
      - Allow Private Key Export: No
   1. Click "Upload Now"
1. After you import the certificate you should see "Warning: Unable to verify the issuer certificate." and the certificate in the list now
1. Next associate the certifcate with servive under services interface
   1. Select the correct service which the certificate is going to be associated with
   1. Then Advanced options click "Show"
   1. Server Name Identification
   1. Under the 'Configured Domains' section click 'Edit' next to the domain to which the certificate is going to be associated.
   1. Select the certificate that you just uploaded
   1. Click 'Done Editing'
   1. Click the button 'Save Changes' at the top of the page
1. Verify that information has been update in confluence
1. Verify that there is monitoring on the certificate with icinga

### Rollback Plan

1. Associate the old certificate to the domain
1. Remove the certificate from the Certificates interface

### Verification Plan

1. Access url and check the certificate to make sure it has been updated and that the expiration date has been updated.
