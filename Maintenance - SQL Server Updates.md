# Maintenance - SQL Server Updates

## Information

![Scope-High](https://img.shields.io/badge/Scope-High-Red?style=flat)
![Risk-Medium](https://img.shields.io/badge/Risk-Medium-Orange?style=flat)
![Impact-Medium](https://img.shields.io/badge/Impact-Medium-Orange?style=flat)

### Description

As SQL is on a Windows based OS we need to updates manunaly to keep the SQL cluster up and running.

### Purpose

This document outlines the proceedure to perform a Windows update on the SQL cluster Windows servers within RDC01.

## Proceedure

### Prerequisites

### Procedure/Implementation Plan

### Rollback Plan

### Verification Plan
